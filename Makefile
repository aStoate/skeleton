all: main

CXX=g++
CXX_FLAGS=-std=c++11


clean: 
	rm -rf bin/* src/*.o tests/*.o

ASS2CPP:=${wildcard src/*.cpp}
ASS2O:=${ASS2CPP:.cpp=.o}
main: ${ASS2O}
	$(CXX) $(CXX_FLAGS) $^ -o bin/$@


MAIN_FILE=src/assignment2.cpp
TEST_FILES:=$(filter-out $(MAIN_FILE), $(ASS2CPP)) ${wildcard tests/*.cpp}
TEST_O:=${TEST_FILES:.cpp=.o}
test: ${TEST_O}
	@echo "\033[0;32m === Unit Tests === \033[0m"
	@$(CXX) $(CXX_FLAGS) $^ -o bin/$@

