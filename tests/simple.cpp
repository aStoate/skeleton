#include "catch/catch.hpp"
#include "../src/simple.hpp"

TEST_CASE( "Simple", "[Simple]" ) {
    Simple simple;
    REQUIRE(simple.return5() == 5);
}
